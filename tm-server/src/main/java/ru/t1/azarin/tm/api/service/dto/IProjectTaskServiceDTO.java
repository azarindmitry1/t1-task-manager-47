package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskServiceDTO {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    void unbindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}
