package ru.t1.azarin.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectShowByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectShowByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
